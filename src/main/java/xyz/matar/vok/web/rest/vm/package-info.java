/**
 * View Models used by Spring MVC REST controllers.
 */
package xyz.matar.vok.web.rest.vm;
